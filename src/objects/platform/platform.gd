extends StaticBody2D

signal platform_left_screen

func _on_visible_viewport_exited(viewport):
    emit_signal("platform_left_screen")
