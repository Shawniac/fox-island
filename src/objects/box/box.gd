extends Area2D

signal box_left_screen

func _ready():
    $animations.play("SETUP")
    $animations.play("hover")

func _on_visible_viewport_exited(viewport):
    emit_signal("box_left_screen")
