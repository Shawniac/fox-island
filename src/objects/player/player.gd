extends KinematicBody2D

var FLOOR_NORMAL = Vector2(0, -1)
var SPEED = Vector2()
var JUMP_FORCE = -1350
var WALK_SPEED = 650
var GRAVITY = 2500
var LIVES = 3
var INVINCIBLE = false

signal hit_box
signal game_over

func _ready():
    $animations.play("SETUP")

func _physics_process(delta):
    # move
    SPEED.y += GRAVITY * delta
    process_user_input() # modifies SPEED
    SPEED = move_and_slide(SPEED, FLOOR_NORMAL)
    
    # manage lives
    if LIVES <= 0:
        $timer_invincible.stop()
        $sprite.modulate = Color(1, 0.15, 0.15)
        $animations.stop()
        set_physics_process(false)
        emit_signal("game_over")

func process_user_input():
    if is_on_floor() and Input.is_action_pressed("player_jump"):
        SPEED.y = JUMP_FORCE
        $animations.play("flip")

func hit_box():
    if not INVINCIBLE:   
        LIVES -= 1
        emit_signal("hit_box")
        INVINCIBLE = true
        $sprite.modulate = Color(1, 1, 1, 0.15)
        $timer_invincible.start()

func _on_timer_invincible_timeout():
    INVINCIBLE = false
    $sprite.modulate = Color(1, 1, 1)    
func _on_box1_body_entered(body):
    hit_box()
func _on_box2_body_entered(body):
    hit_box()
