extends Node2D

var SCORE = 0
var GAME_OVER = false

# engine functions
func _ready():
    # randomize the RNG's seed
    randomize()
    
    $gui/score.text = "Score: " + str(SCORE)
    $gui/lives.text = "Lives: " + str($player.LIVES)
func _process(delta):
    if GAME_OVER:
        return
    
    $platform1.position.x -= $player.WALK_SPEED*delta
    $platform2.position.x -= $player.WALK_SPEED*delta
    $platform3.position.x -= $player.WALK_SPEED*delta
    
    $box1.position.x -= $player.WALK_SPEED*delta
    $box2.position.x -= $player.WALK_SPEED*delta
func _input(event):
    if GAME_OVER and event.is_action_pressed("player_jump"):
        get_tree().reload_current_scene()

# game functions
func respawn_box(box):
    var respawn_point
    var slot = randi() % 2
    if slot == 0:
        respawn_point = $spawns/box_high
    else:
        respawn_point = $spawns/box_low
    
    box.position = respawn_point.position
func respawn_platform(platform):
    if platform.name == "platform1":
        platform.position = $spawns/platform.position
    elif platform.name == "platform2":
        platform.position.x = $platform1.position.x + $platform1/sprite.texture.get_width()
    elif platform.name == "platform3":
        platform.position.x = $platform2.position.x + $platform2/sprite.texture.get_width()

# callbacks
func _on_box_box_left_screen():
    SCORE += 100
    $gui/score.text = "Score: " + str(SCORE)
    respawn_box($box1)
func _on_box2_box_left_screen():
    SCORE += 100
    $gui/score.text = "Score: " + str(SCORE)
    respawn_box($box2)

func _on_platform_platform_left_screen():
    respawn_platform($platform1)
func _on_platform2_platform_left_screen():
    respawn_platform($platform2)
func _on_platform3_platform_left_screen():
    respawn_platform($platform3)

func _on_player_hit_box():
    $gui/lives.text = "Lives: " + str($player.LIVES)
func _on_player_game_over():
    GAME_OVER = true
