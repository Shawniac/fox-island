extends Node2D

func _on_play_button_pressed():
    get_tree().change_scene("res://screens/game/game.tscn")

func _on_option_button_pressed():
    get_tree().change_scene("res://screens/options/options.tscn")

func _on_exit_button_pressed():
    get_tree().quit()
