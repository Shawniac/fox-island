extends Node2D

var resolutions = [Vector2(2560, 1440), Vector2(1920, 1080), Vector2(1280, 720)]

func _ready():
    for i in range(resolutions.size()):
        var res = str(resolutions[i].x) + 'x' + str(resolutions[i].y)
        $resolution/resolutions.add_item(res, i)

func _on_fullscreen_pressed():
    OS.window_fullscreen = !OS.window_fullscreen

func _on_borderless_pressed():
    OS.window_borderless = !OS.window_borderless

func _on_confirm_pressed():
    var rp = $resolution/resolutions # get a reference to the resolution picker
    var res_sel = rp.get_item_text(rp.get_selected_id()) # ex. "1920x1080"
    var res = res_sel.split('x')                         # -> [1920, 1080]
    var size = Vector2(res[0], res[1])
    
    # if we're not in fullscreen mode, just change the window size
    # else if we are, change the viewport's size instead
    if !OS.window_fullscreen:
        OS.window_size = size
    else:
        get_viewport().size = size 

func _on_back_pressed():
    get_tree().change_scene("res://screens/main_menu/main_menu.tscn")
